nagiosplugin (1.3.2-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 14 Oct 2022 11:19:08 +0100

nagiosplugin (1.3.2-2) unstable; urgency=medium

  * Team upload.
  * Source-only upload. No changes.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Sun, 14 Feb 2021 12:40:44 -0500

nagiosplugin (1.3.2-1) unstable; urgency=medium

  * Team upload.

  [ Louis-Philippe Véronneau ]
  * New upstream release.
  * d/watch: migrate to github tarball to get tests.
  * d/rules: run unit tests during build.
  * d/control: use dh 13.
  * d/control: update Homepage link.
  * d/control: Standards-Version update to 4.5.1. Add Rules-Requires-Root.
  * d/tests: run the upstream testsuite as an autopkgtest.
  * delete old python2 file.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Fri, 12 Feb 2021 23:44:15 -0500

nagiosplugin (1.2.4-2) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Thu, 25 Jul 2019 01:12:34 +0200

nagiosplugin (1.2.4-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Jan Dittberner ]
  * Fix "python3-nagiosplugin; missing dependencies" by fixing a typo in
    debian/control stanza (Closes: #867410)
  * Fix spelling in debian/control long descriptions
  * Add myself to Uploaders field in debian/control
  * Update debian/copyright
  * Bump Standards-Version to 4.0.0
    - change copyright format URL to https
  * New upstream release

 -- Jan Dittberner <jandd@debian.org>  Tue, 18 Jul 2017 15:25:45 +0200

nagiosplugin (1.2.2-1) unstable; urgency=medium

  * New upstream release

 -- Jordan Metzmeier <jmetzmeier01@gmail.com>  Tue, 03 Jun 2014 12:29:35 -0500

nagiosplugin (1.2.1-1) unstable; urgency=medium

  * New upstream release
  * Update watch file to use the tarball instead of the zip archive
  * Switch from python_distutils to pybuild
  * Build the python3 version of the library

 -- Jordan Metzmeier <jmetzmeier01@gmail.com>  Wed, 19 Mar 2014 22:45:52 -0500

nagiosplugin (1.2-1) unstable; urgency=low

  * Initial release (Closes: #733822).

 -- Jordan Metzmeier <jmetzmeier01@gmail.com>  Tue, 31 Dec 2013 13:39:12 -0600
