Source: nagiosplugin
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Jordan Metzmeier <jmetzmeier01@gmail.com>,
           Jan Dittberner <jandd@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-setuptools,
               python3-pytest,
Standards-Version: 4.6.1
Homepage: https://github.com/mpounsett/nagiosplugin
Vcs-Git: https://salsa.debian.org/python-team/packages/nagiosplugin.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/nagiosplugin
Rules-Requires-Root: no

Package: python3-nagiosplugin
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Python class library for writing Nagios (Icinga) plugins (Python 3)
 nagiosplugin is a Python class library which helps writing Nagios
 (or Icinga) compatible plugins easily in Python. It cares for much of
 the boilerplate code and default logic commonly found in Nagios
 checks, including:
 .
  - Nagios 3 Plugin API compliant parameters and output formatting
  - Full Nagios range syntax support
  - Automatic threshold checking
  - Multiple independent measures
  - Custom status line to communicate the main point quickly
  - Long output and performance data
  - Timeout handling
  - Persistent "cookies" to retain state information between check runs
  - Resume log file processing at the point where the last run left
  - No dependencies beyond the Python standard library (except for Python 2.6).
 .
 This is the Python 3 version of the package.
